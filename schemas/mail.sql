-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 10.0.1.68    Database: kdns
-- ------------------------------------------------------
-- Server version	5.5.5-10.2.11-MariaDB-10.2.11+maria~stretch-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `email_confirmations`
--

DROP TABLE IF EXISTS `email_confirmations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email_confirmations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `usersId` int(10) unsigned NOT NULL,
  `code` char(32) NOT NULL,
  `createdAt` int(10) unsigned NOT NULL,
  `modifiedAt` int(10) unsigned DEFAULT NULL,
  `confirmed` char(1) DEFAULT 'N',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `email_confirmations`
--

LOCK TABLES `email_confirmations` WRITE;
/*!40000 ALTER TABLE `email_confirmations` DISABLE KEYS */;
/*!40000 ALTER TABLE `email_confirmations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `failed_logins`
--

DROP TABLE IF EXISTS `failed_logins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `failed_logins` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `usersId` int(10) unsigned DEFAULT NULL,
  `ipAddress` char(15) NOT NULL,
  `attempted` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `usersId` (`usersId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_logins`
--

LOCK TABLES `failed_logins` WRITE;
/*!40000 ALTER TABLE `failed_logins` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_logins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_changes`
--

DROP TABLE IF EXISTS `password_changes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_changes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `usersId` int(10) unsigned NOT NULL,
  `ipAddress` char(15) NOT NULL,
  `userAgent` text NOT NULL,
  `createdAt` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `usersId` (`usersId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_changes`
--

LOCK TABLES `password_changes` WRITE;
/*!40000 ALTER TABLE `password_changes` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_changes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `profilesId` int(10) unsigned NOT NULL,
  `resource` varchar(16) NOT NULL,
  `action` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `profilesId` (`profilesId`)
) ENGINE=InnoDB AUTO_INCREMENT=316 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` VALUES (84,3,'users','changePassword'),(90,2,'users','changePassword'),(272,1,'users','index'),(274,1,'users','search'),(276,1,'users','edit'),(278,1,'users','create'),(280,1,'users','delete'),(282,1,'users','changePassword'),(284,1,'profiles','index'),(286,1,'profiles','search'),(288,1,'profiles','edit'),(290,1,'profiles','create'),(292,1,'profiles','delete'),(294,1,'permissions','index');
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `profiles`
--

DROP TABLE IF EXISTS `profiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profiles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `active` char(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `active` (`active`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `profiles`
--

LOCK TABLES `profiles` WRITE;
/*!40000 ALTER TABLE `profiles` DISABLE KEYS */;
INSERT INTO `profiles` VALUES (1,'Administrators','Y'),(2,'Mailbox','Y'),(3,'Domain','Y'),(4,'Read-Only','Y');
/*!40000 ALTER TABLE `profiles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `remember_tokens`
--

DROP TABLE IF EXISTS `remember_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `remember_tokens` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `usersId` int(10) unsigned NOT NULL,
  `token` char(32) NOT NULL,
  `userAgent` text NOT NULL,
  `createdAt` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `token` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `remember_tokens`
--

LOCK TABLES `remember_tokens` WRITE;
/*!40000 ALTER TABLE `remember_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `remember_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reset_passwords`
--

DROP TABLE IF EXISTS `reset_passwords`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reset_passwords` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `usersId` int(10) unsigned NOT NULL,
  `code` varchar(48) NOT NULL,
  `createdAt` int(10) unsigned NOT NULL,
  `modifiedAt` int(10) unsigned DEFAULT NULL,
  `reset` char(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `usersId` (`usersId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reset_passwords`
--

LOCK TABLES `reset_passwords` WRITE;
/*!40000 ALTER TABLE `reset_passwords` DISABLE KEYS */;
/*!40000 ALTER TABLE `reset_passwords` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `success_logins`
--

DROP TABLE IF EXISTS `success_logins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `success_logins` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `usersId` int(10) unsigned NOT NULL,
  `ipAddress` char(15) NOT NULL,
  `userAgent` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `usersId` (`usersId`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` char(60) NOT NULL,
  `mustChangePassword` char(1) DEFAULT NULL,
  `profilesId` int(10) unsigned NOT NULL,
  `banned` char(1) NOT NULL,
  `suspended` char(1) NOT NULL,
  `active` char(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `profilesId` (`profilesId`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (5,'Kai Pazdzewicz','ich@kai.cool','$2y$08$aDV6RE8zNVZTUForS3owd.8g9.YAEKDiZqcvQGgEai5es3gNKdEVW','N',1,'N','N','Y');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-08-12 10:54:45


--
-- Table structure for table `domain`
--

DROP TABLE IF EXISTS `domain`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `domain` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `domain` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `max_aliases` int(11) NOT NULL DEFAULT '0',
  `max_mailboxes` int(11) NOT NULL DEFAULT '0',
  `max_quota` bigint(20) NOT NULL DEFAULT '0',
  `transport` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'virtual',
  `backupmx` tinyint(1) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `domain`
--

LOCK TABLES `domain` WRITE;
/*!40000 ALTER TABLE `domain` DISABLE KEYS */;
INSERT INTO `domain` VALUES (1,'peppermint.cloud','3',0,0,0,'lmtps:unix:private/dovecot-lmtp',0,1,'2018-01-07 00:23:50','2018-05-16 21:26:05'),(2,'kai.cool','0',0,0,0,'lmtps:unix:private/dovecot-lmtp',0,1,'2018-01-07 01:20:13','2018-05-16 21:25:40'),(3,'fastnameserver.eu','0',0,0,0,'lmtps:unix:private/dovecot-lmtp',0,1,'2018-01-07 01:20:52','2018-05-16 21:25:55'),(4,'pazdzewicz.de','0',0,0,0,'lmtps:unix:private/dovecot-lmtp',0,1,'2018-01-07 15:51:17','2018-05-16 21:25:59'),(5,'fahrschule-fleischmann.eu','0',0,0,0,'lmtps:unix:private/dovecot-lmtp',0,1,'2018-01-09 21:12:13','2018-05-16 21:25:49'),(24,'forever-rudi.de','',0,0,0,'lmtps:unix:private/dovecot-lmtp',0,1,'2018-07-30 19:23:41',NULL),(29,'vaperia.de','0',0,0,0,'lmtps:unix:private/dovecot-lmtp',0,1,'2019-01-14 17:06:00',NULL),(30,'mainhattan-capital.de','0',0,0,0,'lmtps:unix:private/dovecot-lmtp',0,1,'0000-00-00 00:00:00',NULL);
/*!40000 ALTER TABLE `domain` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alias`
--

DROP TABLE IF EXISTS `alias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alias` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `goto` longtext COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL,
  `modified` datetime DEFAULT NULL,
  `domain_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alias`
--

LOCK TABLES `alias` WRITE;
/*!40000 ALTER TABLE `alias` DISABLE KEYS */;
INSERT INTO `alias` VALUES (1,'kai@peppermint.cloud','kai@peppermint.cloud',1,'2018-01-07 00:24:07',NULL,1),(2,'postmaster@peppermint.cloud','kai@peppermint.cloud',1,'2018-01-07 00:59:52',NULL,1),(3,'ich@kai.cool','ich@kai.cool',1,'2018-01-07 01:20:32',NULL,2),(6,'kai@pazdzewicz.de','kai@pazdzewicz.de',1,'2018-01-07 15:51:28',NULL,4),(7,'telekom@pazdzewicz.de','telekom@pazdzewicz.de',1,'2018-01-07 15:51:41',NULL,4),(8,'abuse@pazdzewicz.de','mail@fastnameserver.eu',1,'2018-01-07 15:52:03','2018-02-11 17:53:57',4),(10,'ssl@fastnameserver.eu','ssl@fastnameserver.eu',1,'2018-01-07 16:01:51',NULL,3),(11,'kai@fastnameserver.eu','kai@fastnameserver.eu',1,'2018-01-07 16:16:24',NULL,3),(13,'freifunk@pazdzewicz.de','freifunk@pazdzewicz.de',1,'2018-01-08 20:44:28',NULL,4),(14,'@pazdzewicz.de','kai@pazdzewicz.de',1,'2018-01-08 20:47:04',NULL,4),(15,'kontakt@fahrschule-fleischmann.eu','kontakt@fahrschule-fleischmann.eu',1,'2018-01-09 21:12:54',NULL,5),(16,'rudi@pazdzewicz.de','rudi@pazdzewicz.de',1,'2018-01-09 21:35:12',NULL,4),(17,'info@fahrschule-fleischmann.eu','kontakt@fahrschule-fleischmann.eu',1,'2018-01-09 22:15:37',NULL,5),(18,'registry@peppermint.cloud','registry@peppermint.cloud',1,'2018-01-10 10:58:21',NULL,1),(19,'docker@peppermint.cloud','docker@peppermint.cloud',1,'2018-01-10 10:59:04',NULL,1),(22,'mail@fastnameserver.eu','mail@fastnameserver.eu',1,'2018-01-12 21:54:22',NULL,3),(23,'@fastnameserver.eu','mail@fastnameserver.eu',1,'2018-01-12 21:54:34',NULL,3),(24,'exodus@fastnameserver.eu','exodus@fastnameserver.eu',1,'2018-01-17 18:36:08',NULL,3),(26,'herbert@spoc.me','herbert@spoc.me',1,'2018-05-03 22:09:36',NULL,8),(28,'test@peppermint.cloud','test@peppermint.cloud',1,'2018-05-13 17:20:49',NULL,1),(30,'mailbox1@mail.pep','mailbox1@mail.pep',1,'2018-05-16 21:26:42',NULL,10),(34,'test@mail.pep','test@mail.pep',1,'2018-05-16 22:22:37',NULL,10),(36,'kai@mail.pep','kai@mail.pep',1,'2018-05-16 11:18:09',NULL,10),(42,'uups@mail.pep','uups@mail.pep',1,'2018-05-16 11:34:59',NULL,10),(44,'wiiip@mail.pep','wiiip@mail.pep',1,'2018-05-16 11:36:25',NULL,10),(46,'asdfad@mail.pep','asdfad@mail.pep',1,'2018-05-16 11:37:31',NULL,10),(80,'keineantwort@fahrschule-fleischmann.eu','keineantwort@fahrschule-fleischmann.eu',1,'2018-06-01 21:02:27',NULL,5),(82,'noreply@cloudporn.ml','noreply@cloudporn.ml',1,'2018-06-03 14:07:18',NULL,22),(84,'serverdiscounter@pazdzewicz.de','serverdiscounter@pazdzewicz.de',1,'2018-06-25 17:29:19',NULL,4),(86,'kpki@fastnameserver.eu','kpki@fastnameserver.eu',1,'2018-07-12 20:07:13',NULL,3),(88,'dns@fastnameserver.eu','dns@fastnameserver.eu',1,'2018-08-09 12:19:29',NULL,3),(90,'my@peppermint.cloud','my@peppermint.cloud',1,'2018-08-25 21:12:32',NULL,1),(92,'support@free-teamspeak.com','support@free-teamspeak.com',1,'2018-09-22 00:22:35',NULL,28),(94,'@castv.ru','ich@kai.cool',1,'2018-10-12 13:05:58',NULL,26),(95,'monitoring@fastnameserver.eu','monitoring@fastnameserver.eu',1,'0000-00-00 00:00:00',NULL,3),(99,'info@vaperia.de','info@vaperia.de',1,'0000-00-00 00:00:00',NULL,29),(100,'ralf@vaperia.de','ralf@vaperia.de',1,'0000-00-00 00:00:00',NULL,29),(101,'frank@vaperia.de','frank@vaperia.de',1,'0000-00-00 00:00:00',NULL,29);
/*!40000 ALTER TABLE `alias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mailbox`
--

DROP TABLE IF EXISTS `mailbox`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mailbox` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alt_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `quota` bigint(20) NOT NULL DEFAULT '0',
  `local_part` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `access_restriction` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'ALL',
  `homedir` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `maildir` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `uid` bigint(20) DEFAULT NULL,
  `gid` bigint(20) DEFAULT NULL,
  `homedir_size` bigint(20) DEFAULT NULL,
  `maildir_size` bigint(20) DEFAULT NULL,
  `size_at` datetime DEFAULT NULL,
  `delete_pending` tinyint(1) DEFAULT '0',
  `created` datetime NOT NULL,
  `modified` datetime DEFAULT NULL,
  `domain_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mailbox`
--

LOCK TABLES `mailbox` WRITE;
/*!40000 ALTER TABLE `mailbox` DISABLE KEYS */;
INSERT INTO `mailbox` VALUES (1,'kai@peppermint.cloud','$6$5UMWvnMvipluUQfH$Dg1HzVMmnRzJJkj0M0WNNSq/LthzJIgFzQZrbJUx0GZLiZQfUaRirDJkIF6q9MmSNq3nspkDGQAiO.n4jnS7..','Kai Pazdzewicz','',0,'kai',1,'ALL','/var/vmail/peppermint.cloud/kai','maildir:/var/vmail/peppermint.cloud/kai/Maildir:LAYOUT=fs',5000,5000,NULL,NULL,NULL,0,'2018-01-07 00:24:07','2018-05-13 15:01:16',1),
(2,'ich@kai.cool','$6$aNBqS65uw5GOTmc2$YxK6ovwXNuPiLIlH0th5.1.MZ7WvAHgtggAEdNlqLE5RVx5M/tFo666.5fLP3/6o7hd3ld7hH/NFQjn4eyuX5/','kai','',0,'ich',1,'ALL','/var/vmail/kai.cool/ich','maildir:/var/vmail/kai.cool/ich/Maildir:LAYOUT=fs',5000,5000,NULL,NULL,NULL,0,'2018-01-07 01:20:32',NULL,2),(4,'kai@pazdzewicz.de','$6$m02QGVdWW2qwc1NV$GlmqSKWl9xiK/2KIcqnPW.jwnXeRumkV3D4B4DMmX/8WhWDRj4QbnVwBrDIS2yWYfdcOALWZnyLouPdZ0VKxi0','kai','',0,'kai',1,'ALL','/var/vmail/pazdzewicz.de/kai','maildir:/var/vmail/pazdzewicz.de/kai/Maildir:LAYOUT=fs',5000,5000,NULL,NULL,NULL,0,'2018-01-07 15:51:28',NULL,4),(5,'telekom@pazdzewicz.de','$6$PbRYm2Nbej/Ypi3c$fZIk4SOQBjxx/xT.pi/hW2SdPEZZtTPRxmwY3JKDQn7u1Xam/xwY8vIlc3H7TGZEHSRL2j5ft6OnBCDahu7NZ0','telekom','',0,'telekom',1,'ALL','/var/vmail/pazdzewicz.de/telekom','maildir:/var/vmail/pazdzewicz.de/telekom/Maildir:LAYOUT=fs',5000,5000,NULL,NULL,NULL,0,'2018-01-07 15:51:41',NULL,4),(7,'ssl@fastnameserver.eu','$6$ga3XVEsA57nNeLh4$N/8Gbr5ryMRsci4y3N8RgkYaU3CrpsujAbL0BcqjEFTDb1roMO5ZuFOKMoUCUpJFeyN8ChLiH4oK5ScKWPl8a1','ssl','',0,'ssl',1,'ALL','/var/vmail/fastnameserver.eu/ssl','maildir:/var/vmail/fastnameserver.eu/ssl/Maildir:LAYOUT=fs',5000,5000,NULL,NULL,NULL,0,'2018-01-07 16:01:50',NULL,3),(8,'kai@fastnameserver.eu','$6$6aQgVU3jiR4P5Q7D$3Sk25LlBLGO.dpvZvzHIHhaEkdiQanTc35wZyd7hkNMWZp4QhweT4eev6L1ySc29WjyfpP4jXahQsQFwYxxhu1','','',0,'kai',1,'ALL','/var/vmail/fastnameserver.eu/kai','maildir:/var/vmail/fastnameserver.eu/kai/Maildir:LAYOUT=fs',5000,5000,NULL,NULL,NULL,0,'2018-01-07 16:16:24',NULL,3),(10,'freifunk@pazdzewicz.de','$6$bYXXO.e2WbXd8KCT$SmielEFZsjBb8kS7hmgqVs4kXiHDg4NaZ7ZQsBZewzYEPh6i3hAT1wGnUXJbl.aDFbtiMCXFZiD5f/vu1LOyy1','','',0,'freifunk',1,'ALL','/var/vmail/pazdzewicz.de/freifunk','maildir:/var/vmail/pazdzewicz.de/freifunk/Maildir:LAYOUT=fs',5000,5000,NULL,NULL,NULL,0,'2018-01-08 20:44:28',NULL,4),(11,'kontakt@fahrschule-fleischmann.eu','$6$SJDFdnYJwZ./MU4K$jaI42fStQIT4Gn9ZbQ2P02eJuwIgSHWvJNmuJRRaZPQzLvIt3ZMtAUZaal0xxOKhMG9KWnAStQjd93TtjYQrQ0','','',0,'kontakt',1,'ALL','/var/vmail/fahrschule-fleischmann.eu/kontakt','maildir:/var/vmail/fahrschule-fleischmann.eu/kontakt/Maildir:LAYOUT=fs',5000,5000,NULL,NULL,NULL,0,'2018-01-09 21:12:54',NULL,5),(12,'rudi@pazdzewicz.de','$6$CGp5ETfEZyxu8khP$teu1UHtsVuj.ufoGvjxbXCTybTCWv0p2CU85Z4yDy8bVmV0p2B/HOq8Dv6ASeHRE7JMa/Nig2QKDHCGk8rNs5/','','',0,'rudi',1,'ALL','/var/vmail/pazdzewicz.de/rudi','maildir:/var/vmail/pazdzewicz.de/rudi/Maildir:LAYOUT=fs',5000,5000,NULL,NULL,NULL,0,'2018-01-09 21:35:12',NULL,4),(13,'registry@peppermint.cloud','$6$6XVoDKZwi09HmS2g$O4givNzrcOsKlnrLkJvLOC90QaaDEcJmCY0cIVAiShtnQraSXNZgTqOY/HjDaXYL3zYR.j0TzVp.pme8RUedK1','','',0,'registry',1,'ALL','/var/vmail/peppermint.cloud/registry','maildir:/var/vmail/peppermint.cloud/registry/Maildir:LAYOUT=fs',5000,5000,NULL,NULL,NULL,0,'2018-01-10 10:58:21',NULL,1),(14,'docker@peppermint.cloud','$6$Xr7bcj8gJku1Q0D8$gJabLloZWwUjBzCIANtJ/IqzzhJOfdQA2K8l9RQEyEH.4DNkk8.xS9x1paI8PVoVKv1n3FPeH.q9oj5NQ15Do.','','',0,'docker',1,'ALL','/var/vmail/peppermint.cloud/docker','maildir:/var/vmail/peppermint.cloud/docker/Maildir:LAYOUT=fs',5000,5000,NULL,NULL,NULL,0,'2018-01-10 10:59:04',NULL,1),(15,'mail@fastnameserver.eu','$6$EW7twshKGOI8agrA$3M1tnsLWhDaqChf7RQWvi/YLwiTUGzoCQOM8tt3q/hl87FM/Q32n9x953MRzWv1H/QCXXHe7Wira0aTM4Vr.e0','','',0,'mail',1,'ALL','/var/vmail/fastnameserver.eu/mail','maildir:/var/vmail/fastnameserver.eu/mail/Maildir:LAYOUT=fs',5000,5000,NULL,NULL,NULL,0,'2018-01-12 21:54:22',NULL,3),(16,'exodus@fastnameserver.eu','$6$2XrwCYDGGx1oytIM$ZuX9iSt8aVOl5m3jTASwnnzOAvmgDSgMyon3V3Wr8X6nsqqHR11aCijQVvg2p72p/YQVzSDhcP.BSHxXRvFwZ/','Exodus','',0,'exodus',1,'ALL','/var/vmail/fastnameserver.eu/exodus','maildir:/var/vmail/fastnameserver.eu/exodus/Maildir:LAYOUT=fs',5000,5000,NULL,NULL,NULL,0,'2018-01-17 18:36:08',NULL,3),(18,'herbert@spoc.me','$6$GYDgLt9O2KYq0Q2x$OICViUAYJgs6H3BGrSWqprQin/EIh0Icme3Y3QsmlMiRef954BQe3VwOF68LJcEHIDth01PGj/X2kKz18aMSb/','Herbert Lorber','',0,'herbert',1,'ALL','/var/vmail/spoc.me/herbert','maildir:/var/vmail/spoc.me/herbert/Maildir:LAYOUT=fs',5000,5000,NULL,NULL,NULL,0,'2018-05-03 22:09:36','2018-05-03 22:39:06',8),(46,'kai@mail.pep','$6$116d49543e1a1e5e$9.ryQvm2dKO2ifsRC6YP.iVpE3ZohzyxC/o.YeI/DwzSyC.juWPL1TXcvZSud/T4nyMpMY75va5r3q75UvHgK0',NULL,NULL,0,'kai',1,'ALL','/var/vmail/mail.pep/kai','maildir:/var/vmail/mail.pep/kai/Maildir:LAYOUT=fs',5000,5000,NULL,NULL,NULL,0,'2018-05-21 03:46:01',NULL,10),(48,'keineantwort@fahrschule-fleischmann.eu','$6$LvOh1rxF7jtcAKIr$CVF/SSiBbowYIilrpfkjsbMBPU7j0flPizpGbaKNtWQ3IyL5WFxpP6w4w0AoiQlNrSRNhwMyr4aD40tJCCao1.','Fahrschule Fleischmann','',0,'keineantwort',1,'ALL','/var/vmail/fahrschule-fleischmann.eu/keineantwort','maildir:/var/vmail/fahrschule-fleischmann.eu/keineantwort/Maildir:LAYOUT=fs',5000,5000,NULL,NULL,NULL,0,'2018-06-01 21:02:27',NULL,5),(50,'noreply@cloudporn.ml','$6$2rPUnvdKUM0ogghI$NmW8NPLN/LliidiuzIw0.EL5BdLvqjkKZZafIingqOm3x8r2ogHuA8gKUsJINDVLQusU2UFQEtyErO0B9i52F1','','',0,'noreply',1,'ALL','/var/vmail/cloudporn.ml/noreply','maildir:/var/vmail/cloudporn.ml/noreply/Maildir:LAYOUT=fs',5000,5000,NULL,NULL,NULL,0,'2018-06-03 14:07:18',NULL,22),(52,'serverdiscounter@pazdzewicz.de','$6$au/.lWXyUgnMksZZ$C.cyfQ7e.M0ZlmrN1zrSAWlV7Qyg6zl8zUhum5hRTuLRrmaw60TJiPvOAfOMxWiB/KKq4Kc5DAz7mrcU1TJF01','','',0,'serverdiscounter',1,'ALL','/var/vmail/pazdzewicz.de/serverdiscounter','maildir:/var/vmail/pazdzewicz.de/serverdiscounter/Maildir:LAYOUT=fs',5000,5000,NULL,NULL,NULL,0,'2018-06-25 17:29:19',NULL,4),(54,'kpki@fastnameserver.eu','$6$ItelTfvE6p4rWlE4$xhHC8ejcyyNJzR/1bKUg1raFaOGjA3jL9Kmpq.85BcWaI5stQ4M0Jmkdo3vbr1IpwAH.sSsjHLZfcx4OYCPHP/','','',0,'kpki',1,'ALL','/var/vmail/fastnameserver.eu/kpki','maildir:/var/vmail/fastnameserver.eu/kpki/Maildir:LAYOUT=fs',5000,5000,NULL,NULL,NULL,0,'2018-07-12 20:07:12',NULL,3),(56,'dns@fastnameserver.eu','$6$ZgwVTnOCMRdfIAMX$JIyYimDUrybsOqgJ9WJXuiosbD.jcgXm37aGOM1CZ9NR/x0j6qdOkIfE.buGwkmc3O1NKRSoWPmkwYflfPYrg1','','',0,'dns',1,'ALL','/var/vmail/fastnameserver.eu/dns','maildir:/var/vmail/fastnameserver.eu/dns/Maildir:LAYOUT=fs',5000,5000,NULL,NULL,NULL,0,'2018-08-09 12:19:29',NULL,3),(58,'my@peppermint.cloud','{CRYPT}$2y$05$rB54EwzaCbjBXz3KoafKHuW06TlFDCoIsBq6lTUarQmLLDbeFkwSO','My.Peppermint.Cloud','',0,'my',1,'ALL','/var/vmail/peppermint.cloud/my','maildir:/var/vmail/peppermint.cloud/my/Maildir:LAYOUT=fs',5000,5000,NULL,NULL,NULL,0,'2018-08-25 21:12:32',NULL,1),(60,'support@free-teamspeak.com','$6$vzoYTlvEc689jo23$puYxyqZLEFjgK0TTRSxdFx9ATnfgDpADWhTP1QaZUEevP4cAkbkgcmfaJuXkQANrkKn8fmdhXOipoaanXQUps.','','',0,'support',1,'ALL','/var/vmail/free-teamspeak.com/support','maildir:/var/vmail/free-teamspeak.com/support/Maildir:LAYOUT=fs',5000,5000,NULL,NULL,NULL,0,'2018-09-22 00:22:34',NULL,28),(61,'frank@vaperia.de','$2y$05$tWtIMzZwGSI/wNJQnIoMMeuFuiega/dpHO/7rAnUwNl3uW3wPekTC',NULL,NULL,0,'frank',1,'ALL','/var/vmail/vaperia.de/frank','maildir:/var/vmail/vaperia.de/frank/Maildir:LAYOUT=fs',5000,5000,NULL,NULL,NULL,0,'0000-00-00 00:00:00',NULL,29),(62,'ralf@vaperia.de','$2y$05$PNgjl.6.AJRVnjvUBlGKfeXeYDW3aF37Xkvlo2v1gvzgQVZ.WKQEC',NULL,NULL,0,'ralf',1,'ALL','/var/vmail/vaperia.de/ralf','maildir:/var/vmail/vaperia.de/ralf/Maildir:LAYOUT=fs',5000,5000,NULL,NULL,NULL,0,'0000-00-00 00:00:00',NULL,29),(63,'info@vaperia.de','$2y$05$ItgEjzhAQqcVPweOFpFOaOp/zWweE.8Rq9zoPlvRfD6DdbyY2YBVe',NULL,NULL,0,'info',1,'ALL','/var/vmail/vaperia.de/info','maildir:/var/vmail/vaperia.de/info/Maildir:LAYOUT=fs',5000,5000,NULL,NULL,NULL,0,'0000-00-00 00:00:00',NULL,29),(64,'monitoring@fastnameserver.eu','$2y$05$9MikLbKb2deyzW7V836pou0Tzx./U6crKVhm4elJbfW.rUP8OVjGO',NULL,NULL,0,'monitoring',1,'ALL','/var/vmail/fastnameserver.eu/monitoring','maildir:/var/vmail/fastnameserver.eu/monitoring/Maildir:LAYOUT=fs',5000,5000,NULL,NULL,NULL,0,'0000-00-00 00:00:00',NULL,3),(65,'kontakt@mainhattan-capital.de','$2y$05$pobS/kPUSsnIsttWK1Gb9O1KfApSR0an.zVy2uSv5/q./GPLxMTy2',NULL,NULL,0,'kontakt',1,'ALL','/var/vmail/mainhattan-capital.de/kontakt','maildir:/var/vmail/mainhattan-capital.de/kontakt:LAYOUT=fs',5000,5000,NULL,NULL,NULL,0,'0000-00-00 00:00:00',NULL,NULL);
/*!40000 ALTER TABLE `mailbox` ENABLE KEYS */;
UNLOCK TABLES;
