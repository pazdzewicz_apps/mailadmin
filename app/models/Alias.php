<?php
namespace Mailadmin\Models;

use Phalcon\Mvc\Model;
# Validation
use Phalcon\Validation;
use Phalcon\Validation\Validator\Uniqueness;
# Own Models
use Mailadmin\Models\Domain;

/**
 * Mailadmin\Models\Alias
 * All the aliases in the application.
 */
class Alias extends Model
{
  /**
  * ID
  */
  private $id;

  public function getId()
  {
    return (int)$this->id;
  }

  /**
  * address
  */
  private $address;

  public function getAddress()
  {
    return (string)$this->address;
  }

  /**
  * goto
  */
  private $goto;

  public function getGoto()
  {
    return (string)$this->goto;
  }

  public function setGoto($goto)
  {
    $this->goto=$goto;
    return (bool)$this->save();
  }

  /**
  * active
  */
  private $active;

  public function getActive()
  {
    return (bool)$this->active;
  }

  /**
  * created
  */
  private $created;

  public function getCreated()
  {
    return $this->created;
  }

  /**
  * modified
  */
  private $modified;

  public function getModified()
  {
    return $this->modified;
  }

  /**
  * domain_id
  */
  private $domain_id;

  public function getDomain()
  {
    return Domain::findFirst($this->domain_id);
  }

  public function beforeCreate()
  {
    $this->created=date('Y-m-d H:i:s');
    $this->transport='lmtps:unix:private/dovecot-lmtp';
    if($this->getActive()==true)
    {
      $this->active=1;
    }
    else
    {
      $this->active=0;
    }
  }

  public function beforeUpdate()
  {
    $this->modified=date('Y-m-d H:i:s');
  }

  public function validation()
  {
    $validator = new Validation();
    $validator->add('address', new Uniqueness([
      "message" => "The address already exists"
    ]));
    return $this->validate($validator);
  }
}
