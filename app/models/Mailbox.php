<?php
namespace Mailadmin\Models;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Message as Message;
# Validation
use Phalcon\Validation;
use Phalcon\Validation\Validator\Uniqueness;
# Own Models
use Mailadmin\Models\Domain;
use Mailadmin\Models\Alias;

/**
 * Mailadmin\Models\Mailbox
 * All the domains in the application.
 */
class Mailbox extends Model
{
  /**
  * ID
  */
  private $id;

  public function getId()
  {
    return (int)$this->id;
  }

  /**
  * username
  */
  private $username;

  public function getUsername()
  {
    return (string)$this->username;
  }

  /**
  * password
  */
  private $password;

  public function setPassword($password)
  {
    $this->getDI()->getSecurity()->setDefaultHash('CRYPT_SHA512');
    $this->password=$this->getDI()->getSecurity()->hash($password);
    return (bool)$this->save();
  }

  /**
  * name
  */
  private $name;

  public function getName()
  {
    return (string)$this->name;
  }

  public function setName($name)
  {
    $this->name=$name;
    return (bool)$this->save();
  }

  /**
  * quota
  */
  private $quota;

  public function getQuota()
  {
    return (int)$this->quota;
  }

  /**
  * local_part
  */
  private $local_part;

  public function getLocalPart()
  {
    return (string)$this->local_part;
  }

  /**
  * active
  */
  private $active;

  public function getActive()
  {
    return (bool)$this->active;
  }

  /**
  * access_restriction
  */
  private $access_restriction;

  public function getAccessRestriction()
  {
    return (string)$this->access_restriction;
  }

  /**
  * homedir
  */
  private $homedir;

  public function getHomedir()
  {
    return (string)$this->homedir;
  }

  /**
  * maildir
  */
  private $maildir;

  public function getMaildir()
  {
    return (string)$this->maildir;
  }

  /**
  * uid
  */
  private $uid;

  public function getUID()
  {
    return (int)$this->uid;
  }

  /**
  * gid
  */
  private $gid;

  public function getGID()
  {
    return (int)$this->gid;
  }

  /**
  * delete_pending
  */
  private $delete_pending;

  public function getDeletePending()
  {
    return (bool)$this->delete_pending;
  }

  /**
  * created
  */
  private $created;

  public function getCreated()
  {
    return $this->created;
  }

  /**
  * modified
  */
  private $modified;

  public function getModified()
  {
    return $this->modified;
  }

  /**
  * domain_id
  */
  private $domain_id;

  public function getDomain()
  {
    return Domain::findFirst($this->domain_id);
  }

  public function beforeCreate()
  {
    $this->created=date('Y-m-d H:i:s');
    $this->username=$this->getLocalPart()."@".$this->getDomain()->getDomain();
    $this->access_restriction="ALL";
    $this->homedir="/var/vmail/".$this->getDomain()->getDomain()."/".$this->getLocalPart();
    $this->maildir="maildir:".$this->getHomedir()."/Maildir:LAYOUT=fs";
    $this->uid=5000;
    $this->gid=5000;
    if($this->getActive()==true)
    {
      $this->active=1;
    }
    else
    {
      $this->active=0;
    }
    $alias=new Alias([
      'address' => $this->getUsername(),
      'goto' => $this->getUsername(),
      'active' => true,
      'domain_id' => $this->getDomain()->getId(),
      'created' => 'now'
    ]);
    if(!$alias->create())
    {
      $message = new Message("Alias not created", "alias/create", "Invalid");
      $this->appendMessage($message);
      foreach ($alias->getMessages() as $amessage) {
        $message = new Message($amessage->getMessage(), "alias/create", "Invalid");
        $this->appendMessage($message);
      }
      return false;
    }
    else
    {
      $message = new Message("Alias created", "alias/create", "Success");
      $this->appendMessage($message);
    }
  }

  public function beforeUpdate()
  {
    $this->modified=date('Y-m-d H:i:s');
  }

  public function beforeDelete()
  {
    $alias=Alias::findFirst('address LIKE "'.$this->getUsername().'" and goto LIKE "'.$this->getUsername().'"');
    if($alias)
    {
      if(!$alias->delete())
      {
        $message = new Message("Alias not deleted", "alias/delete", "Failed");
        $this->appendMessage($message);
        foreach ($alias->getMessages() as $amessage) {
          $message = new Message($amessage->getMessage(), "alias/delete", "Failed");
          $this->appendMessage($message);
        }
        return false;
      }
      else
      {
        $message = new Message("Alias deleted", "alias/delete", "Success");
        $this->appendMessage($message);
      }
    }
    else
    {
      $message = new Message("Alias not found", "alias/delete", "NotFound");
      $this->appendMessage($message);
    }
  }

  public function validation()
  {
    $validator = new Validation();
    $validator->add('username', new Uniqueness([
      "message" => "The username already exists"
    ]));
    return $this->validate($validator);
  }
}
