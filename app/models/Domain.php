<?php
namespace Mailadmin\Models;

use Phalcon\Mvc\Model;
# Validation
use Phalcon\Validation;
use Phalcon\Validation\Validator\Uniqueness;
# Own Models
use Mailadmin\Models\Mailbox;
use Mailadmin\Models\Alias;

/**
 * Mailadmin\Models\Domains
 * All the domains in the application.
 */
class Domain extends Model
{
  /**
  * ID
  */
  private $id;

  public function getId()
  {
    return (int)$this->id;
  }

  /**
  * domain
  */
  private $domain;

  public function getDomain()
  {
    return (string)$this->domain;
  }

  /**
  * description
  */
  private $description;

  public function getDescription()
  {
    return (string)$this->description;
  }

  public function setDescription($description)
  {
    $this->description=$description;
    return $this->save();
  }

  /**
  * max_aliases
  */
  private $max_aliases;

  public function getMaxAliases()
  {
    return (int)$this->max_aliases;
  }

  public function getAliases()
  {
    return Alias::find('domain_id = '.$this->id);
  }

  /**
  * max_mailboxes
  */
  private $max_mailboxes;

  public function getMaxMailboxes()
  {
    return (int)$this->max_mailboxes;
  }

  public function getMailboxes()
  {
    return Mailbox::find('domain_id = '.$this->id);
  }

  /**
  * max_quota
  */
  private $max_quota;

  public function getMaxQuota()
  {
    return (int)$this->max_quota;
  }

  /**
  * transport
  */
  private $transport;

  public function getTransport()
  {
    return $this->transport;
  }

  /**
  * backupmx
  */
  private $backupmx;

  public function getBackupMX()
  {
    return (bool)$this->backupmx;
  }

  /**
  * active
  */
  private $active;

  public function getActive()
  {
    return (bool)$this->active;
  }

  /**
  * created
  */
  private $created;

  public function getCreated()
  {
    return $this->created;
  }

  /**
  * modified
  */
  private $modified;

  public function getModified()
  {
    return $this->modified;
  }

  public function beforeCreate()
  {
    $this->created=date('Y-m-d H:i:s');
    $this->transport='lmtps:unix:private/dovecot-lmtp';
    if($this->getActive()==true)
    {
      $this->active=1;
    }
    else
    {
      $this->active=0;
    }
  }

  public function beforeUpdate()
  {
    $this->modified=date('Y-m-d H:i:s');
  }

  public function validation()
  {
    $validator = new Validation();
    $validator->add('domain', new Uniqueness([
      "message" => "The domain already exists"
    ]));
    return $this->validate($validator);
  }

  public function initialize()
  {
    $this->hasMany('id', __NAMESPACE__ . '\Mailbox', 'domain_id', [
      'alias' => 'mailboxes',
      'foreignKey' => [
        'message' => 'Domain cannot be deleted, there are still mailboxes.'
      ]
    ]);
    $this->hasMany('id', __NAMESPACE__ . '\Alias', 'domain_id', [
      'alias' => 'aliases',
      'foreignKey' => [
        'message' => 'Domain cannot be deleted, there are still aliases.'
      ]
    ]);
  }
}
