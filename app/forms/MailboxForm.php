<?php
namespace Mailadmin\Forms;

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Submit;
use Phalcon\Forms\Element\Check;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\Numeric;
use Phalcon\Forms\Element\Password;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Identical;
use Phalcon\Validation\Validator\StringLength;
use Phalcon\Validation\Validator\Confirmation;

class MailboxForm extends Form
{
  public function initialize()
  {
    // name
    $name = new Text('name', [
      'placeholder' => 'John Doe',
      'class' => 'form-control'
    ]);
    $name->addValidators([
      new PresenceOf([
        'message' => 'The name is required'
      ])
    ]);
    $name->setLabel('Name');
    $this->add($name);

    // local_part
    $local_part = new Text('local_part', [
      'placeholder' => 'john.doe',
      'class' => 'form-control'
    ]);
    $local_part->addValidators([
      new PresenceOf([
        'message' => 'The username is required'
      ])
    ]);
    $local_part->setLabel('Username');
    $this->add($local_part);

    // quota
    $quota = new Numeric('quota', [
      'value' => '0',
      'min' => '0',
      'class' => 'form-control'
    ]);
    $quota->setLabel('Quota');
    $this->add($quota);

    // active
    $active = new Check('active', [
      'checked' => true
    ]);
    $active->setLabel('Active');
    $this->add($active);

    // Password
    $password = new Password('password', [
      'class' => 'form-control'
    ]);
    $password->setLabel('Password');
    $password->addValidators([
      new PresenceOf([
        'message' => 'The password is required'
      ]),
      new StringLength([
        'min' => 8,
        'messageMinimum' => 'Password is too short. Minimum 8 characters'
      ]),
      new Confirmation([
        'message' => 'Password doesn\'t match confirmation',
        'with' => 'confirmPassword'
      ])
    ]);
    $this->add($password);

    // Confirm Password
    $confirmPassword = new Password('confirmPassword', [
      'class' => 'form-control'
    ]);
    $confirmPassword->setLabel('Confirm Password');
    $confirmPassword->addValidators([
      new PresenceOf([
        'message' => 'The confirmation password is required'
      ])
    ]);
    $this->add($confirmPassword);

    // Go Button
    $this->add(new Submit('go', [
      'class' => 'btn btn-success form-control',
      'value' => '+ Create'
    ]));
  }
}
