<?php
namespace Mailadmin\Forms;

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Submit;
use Phalcon\Forms\Element\Password;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Identical;
use Phalcon\Validation\Validator\StringLength;
use Phalcon\Validation\Validator\Confirmation;

class MailboxPasswordForm extends Form
{
  public function initialize()
  {
    // Password
    $password = new Password('password', [
      'class' => 'form-control'
    ]);
    $password->setLabel('Password');
    $password->addValidators([
      new PresenceOf([
        'message' => 'The password is required'
      ]),
      new StringLength([
        'min' => 8,
        'messageMinimum' => 'Password is too short. Minimum 8 characters'
      ]),
      new Confirmation([
        'message' => 'Password doesn\'t match confirmation',
        'with' => 'confirmPassword'
      ])
    ]);
    $this->add($password);

    // Confirm Password
    $confirmPassword = new Password('confirmPassword', [
      'class' => 'form-control'
    ]);
    $confirmPassword->setLabel('Confirm Password');
    $confirmPassword->addValidators([
      new PresenceOf([
        'message' => 'The confirmation password is required'
      ])
    ]);
    $this->add($confirmPassword);

    // Go Button
    $this->add(new Submit('go', [
      'class' => 'btn btn-success form-control',
      'value' => 'Save'
    ]));
  }
}
