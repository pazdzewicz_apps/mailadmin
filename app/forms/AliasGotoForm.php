<?php
namespace Mailadmin\Forms;

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Submit;
use Phalcon\Validation\Validator\PresenceOf;

class AliasGotoForm extends Form
{
  public function initialize()
  {
    // goto
    $goto = new Text('goto', [
      'placeholder' => 'destination@example.com',
      'class' => 'form-control'
    ]);
    $goto->addValidators([
      new PresenceOf([
        'message' => 'The destination is required'
      ])
    ]);
    $goto->setLabel('Destination (empty is catchall)');
    $this->add($goto);
    // Go Button
    $this->add(new Submit('go', [
      'class' => 'btn btn-success form-control',
      'value' => '+ Create'
    ]));
  }
}
