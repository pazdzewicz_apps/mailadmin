<?php
namespace Mailadmin\Forms;

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\TextArea;
use Phalcon\Forms\Element\Submit;
use Phalcon\Forms\Element\Check;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\Numeric;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Identical;

class DomainForm extends Form
{
  public function initialize()
  {
    // domain
    $domain = new Text('domain', [
      'placeholder' => 'example.com',
      'class' => 'form-control'
    ]);
    $domain->addValidators([
      new PresenceOf([
        'message' => 'The domain is required'
      ])
    ]);
    $domain->setLabel('Domain');
    $this->add($domain);

    // description
    $description = new TextArea('description', [
      'placeholder' => 'my cool domain',
      'class' => 'form-control'
    ]);
    $description->setLabel('Description');
    $this->add($description);

    // max_aliases
    $max_aliases = new Numeric('max_aliases', [
      'value' => '0',
      'min' => '0',
      'class' => 'form-control'
    ]);
    $max_aliases->setLabel('Max Aliases (0 = Unlimited)');
    $this->add($max_aliases);

    // max_mailboxes
    $max_mailboxes = new Numeric('max_mailboxes', [
      'value' => '0',
      'min' => '0',
      'class' => 'form-control'
    ]);
    $max_mailboxes->setLabel('Max Mailboxes (0 = Unlimited)');
    $this->add($max_mailboxes);

    // max_quota
    $max_quota = new Numeric('max_quota', [
      'value' => '0',
      'min' => '0',
      'class' => 'form-control'
    ]);
    $max_quota->setLabel('Max Quota (0 = Unlimited)');
    $this->add($max_quota);

    // active
    $active = new Check('active', [
      'checked' => 'true'
    ]);
    $active->setLabel('Active');
    $this->add($active);

    // backupmx
    $backupmx = new Check('backupmx', [
      'checked' => 'false'
    ]);
    $backupmx->setLabel('Backup MX');
    $this->add($backupmx);

    // Go Button
    $this->add(new Submit('go', [
      'class' => 'btn btn-success form-control',
      'value' => '+ Create'
    ]));
  }
}
