<?php
namespace Mailadmin\Forms;

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Submit;
use Phalcon\Forms\Element\Check;
use Phalcon\Validation\Validator\PresenceOf;

class AliasForm extends Form
{
  public function initialize()
  {
    // local_part
    $local_part = new Text('local_part', [
      'placeholder' => 'redirect',
      'class' => 'form-control'
    ]);
    $local_part->setLabel('Username');
    $this->add($local_part);

    // goto
    $goto = new Text('goto', [
      'placeholder' => 'destination@example.com',
      'class' => 'form-control'
    ]);
    $goto->addValidators([
      new PresenceOf([
        'message' => 'The destination is required'
      ])
    ]);
    $goto->setLabel('Destination (empty is catchall)');
    $this->add($goto);

    // active
    $active = new Check('active', [
      'checked' => true
    ]);
    $active->setLabel('Active');
    $this->add($active);

    // Go Button
    $this->add(new Submit('go', [
      'class' => 'btn btn-success form-control',
      'value' => '+ Create'
    ]));
  }
}
