<?php
namespace Mailadmin\Forms;

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Submit;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Identical;

class MailboxNameForm extends Form
{
  public function initialize()
  {
    // name
    $name = new Text('name', [
      'placeholder' => 'John Doe',
      'class' => 'form-control'
    ]);
    $name->addValidators([
      new PresenceOf([
        'message' => 'The name is required'
      ])
    ]);
    $name->setLabel('Name');
    $this->add($name);

    // Go Button
    $this->add(new Submit('go', [
      'class' => 'btn btn-success form-control',
      'value' => 'Save'
    ]));
  }
}
