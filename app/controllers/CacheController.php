<?php
namespace Mailadmin\Controllers;

use Phalcon\Paginator\Adapter\NativeArray as PaginatorArray;

class CacheController extends ControllerBase
{
  public function initialize()
  {
    $this->view->setVar('logged_in', is_array($this->auth->getIdentity()));
    $this->view->setTemplateBefore('private');
  }

  public function indexAction()
  {
    if(empty($this->request->get("page")))
    {
      $currentPage = 0;
    }
    else
    {
      $currentPage = (int) $this->request->get("page");
    }
    $cached=$this->cache->queryKeys('mailadmin_');
    $paginator=new PaginatorArray(
      [
        "data"  => $cached,
        "limit" => 10,
        "page"  => $currentPage,
      ]
    );
    $this->view->caches=$paginator->getPaginate();
  }

  public function viewAction($key=null)
  {
    if($key==null)
    {
      $this->flash->error("Cache Key not found.");
      return $this->dispatcher->forward([
        'controller' => 'cache',
        'action' => 'index'
      ]);
    }
    if(!$this->cache->exists($key))
    {
      $this->flash->error("Cache Key not found.");
      return $this->dispatcher->forward([
        'controller' => 'cache',
        'action' => 'index'
      ]);
    }
    $cached=$this->cache->get($key);
    ob_start();
    var_dump(json_decode(json_encode($cached,true)));
    $this->view->cached=ob_get_clean();
    $this->view->key=$key;
  }

  public function deleteAction($key=null)
  {
    if(!$this->cache->exists($key))
    {
      // Delete all Cache Keys
      foreach ($this->cache->queryKeys() as $key)
      {
        if($this->cache->delete($key))
        {
          $this->flash->success("Cache Key '".$key."' deleted.");
        }
        else
        {
          $this->flash->error("Cache Key '".$key."' not deleted.");
        }
      }
    }
    else
    {
      if($this->cache->delete($key))
      {
        $this->flash->success("Cache Key '".$key."' deleted.");
      }
      else
      {
        $this->flash->error("Cache Key '".$key."' not deleted.");
      }
    }
    return $this->dispatcher->forward([
      'controller' => 'cache',
      'action' => 'index'
    ]);
  }
}
