<?php
namespace Mailadmin\Controllers;

# Own Models
use Mailadmin\Models\Alias;
use Mailadmin\Models\Domain;
# Own Forms
use Mailadmin\Forms\AliasForm;
use Mailadmin\Forms\AliasGotoForm;
# Other
use Phalcon\Paginator\Adapter\Model as PaginatorModel;

class AliasController extends ControllerBase
{
  /**
  * Default action. Set the public layout (layouts/private.volt)
  */
  public function initialize()
  {
    $this->view->setVar('logged_in', is_array($this->auth->getIdentity()));
    $this->view->setTemplateBefore('private');
  }

  public function indexAction()
  {
    # Get Page for Paginator
    if(empty($this->request->get("page")))
    {
      $currentPage = 0;
    }
    else
    {
      $currentPage = (int) $this->request->get("page");
    }
    # Get Paginator Data
    $cache_key="mailadmin_alias_index.cache";
    $alias=$this->cache->get($cache_key);
    if($alias===null)
    {
      $alias=Alias::find();
      $this->cache->save($cache_key,$alias);
    }
    # Create Paginator
    $paginator=new PaginatorModel(
      [
        "data"  => $alias,
        "limit" => 10,
        "page"  => $currentPage,
      ]
    );
    $this->view->aliases=$paginator->getPaginate();
  }

  public function createAction($domain_id=null)
  {
    if($domain_id===null)
    {
      $this->flash->error("Domain not found!");
      return $this->dispatcher->forward([
          "action" => "index"
      ]);
    }
    $cache_key="mailadmin_domain_view_".$domain_id.".cache";
    $domain=$this->cache->get($cache_key);
    if($domain===null)
    {
      $domain=Domain::findFirst($domain_id);
      if($domain===false)
      {
        $this->flash->error("Domain not found!");
        return $this->dispatcher->forward([
            "action" => "index"
        ]);
      }
      $this->cache->save($cache_key,$domain);
    }

    $form = new AliasForm();
    if($this->request->isPost())
    {
      if($form->isValid($this->request->getPost())!=false)
      {
        if($this->request->getPost('active')=="on")
        {
          $active=true;
        }
        else
        {
          $active=false;
        }
        if($this->request->getPost('local_part')."@".$domain->getDomain() != $this->request->getPost('goto'))
        {
          $alias = new Alias([
            'address' => $this->request->getPost('local_part')."@".$domain->getDomain(),
            'goto' => $this->request->getPost('goto'),
            'active' => $active,
            'domain_id' => $domain->getId(),
            'created' => 'now'
          ]);
          if($alias->save())
          {
            $this->flash->success("Alias created");
            // Clear Cache
            $this->cache->save($cache_key,null);
            $this->cache->save("mailadmin_alias_index.cache",null);
            $this->cache->save("mailadmin_alias_index_".$alias->getDomain()->getId().".cache",null);
            // Forward to Mailbox site
            return $this->dispatcher->forward([
              'action' => 'view',
              'params' => [$alias->getId()]
            ]);
          }
          $this->flash->error($alias->getMessages());
        }
        else {
          $this->flash->error("Username can't be the same as destination");
        }
      }
      else
      {
        $this->flash->error("Submitted Data not valid");
        foreach($form->getMessages() as $message)
        {
          // Displays all Warnings from the validation
          $this->flash->warning($message);
        }
      }
    }

    $this->view->domain=$domain;
    $this->view->form=$form;
  }

  public function viewAction($id=null)
  {
    if($id===null)
    {
      $this->flash->error("Alias not found!");
      return $this->dispatcher->forward([
          "action" => "index"
      ]);
    }
    $cache_key="mailadmin_alias_view_".$id.".cache";
    $alias=$this->cache->get($cache_key);
    if($alias===null)
    {
      $alias=Alias::findFirst($id);
      if($alias===false)
      {
        $this->flash->error("Alias not found!");
        return $this->dispatcher->forward([
            "action" => "index"
        ]);
      }
      $this->cache->save($cache_key,$alias);
    }
    $this->view->alias=$alias;
  }

  public function deleteAction($id=null)
  {
    if($id===null)
    {
      $this->flash->error("Alias not found!");
      return $this->dispatcher->forward([
          "action" => "index"
      ]);
    }
    $cache_key="mailadmin_alias_view_".$id.".cache";
    $alias=$this->cache->get($cache_key);
    if($alias===null)
    {
      $alias=Alias::findFirst($id);
      if($alias===false)
      {
        $this->flash->error("Alias not found!");
        return $this->dispatcher->forward([
            "action" => "index"
        ]);
      }
      $this->cache->save($cache_key,$alias);
    }
    if($this->request->isPost())
    {
      if($alias->delete())
      {
        $this->flash->success("Alias deleted!");
        // Clear Cache
        $this->cache->save($cache_key,null);
        $this->cache->save("mailadmin_alias_index.cache",null);
        $this->cache->save("mailadmin_alias_index_".$alias->getDomain()->getId().".cache",null);
        return $this->dispatcher->forward([
            "controller" => "domain",
            "action" => "view",
            "params" => [$alias->getDomain()->getId()]
        ]);
      }
      else
      {
        $this->flash->error("Alias can't be deleted");
        foreach($alias->getMessages() as $message)
        {
          // Displays all Warnings from the validation
          $this->flash->warning($message);
        }
      }
    }
    $this->view->alias=$alias;
  }

  public function changeGotoAction($id=null)
  {
    if($id===null)
    {
      $this->flash->error("Alias not found!");
      return $this->dispatcher->forward([
          "action" => "index"
      ]);
    }
    $cache_key="mailadmin_alias_view_".$id.".cache";
    $alias=$this->cache->get($cache_key);
    if($alias===null)
    {
      $alias=Alias::findFirst($id);
      if($alias===false)
      {
        $this->flash->error("Alias not found!");
        return $this->dispatcher->forward([
            "action" => "index"
        ]);
      }
      $this->cache->save($cache_key,$alias);
    }
    $form = new AliasGotoForm($alias);
    if($this->request->isPost())
    {
      if($form->isValid($this->request->getPost())!=false)
      {
        if($alias->setGoto($this->request->getPost('goto')))
        {
          $this->flash->success("Alias changed!");
          // Clear Cache
          $this->cache->save($cache_key,null);
          $this->cache->save("mailadmin_alias_index.cache",null);
          $this->cache->save("mailadmin_alias_index_".$alias->getDomain()->getId().".cache",null);
          return $this->dispatcher->forward([
              "controller" => "alias",
              "action" => "view",
              "params" => [$alias->getId()]
          ]);
        }
        else
        {
          $this->flash->error("Alias can't be changed");
          foreach($alias->getMessages() as $message)
          {
            // Displays all Warnings from the validation
            $this->flash->warning($message);
          }
        }
      }
      else
      {
        $this->flash->error("Alias can't be changed");
        foreach($alias->getMessages() as $message)
        {
          // Displays all Warnings from the validation
          $this->flash->warning($message);
        }
      }
    }
    $this->view->alias=$alias;
    $this->view->form=$form;
  }
}
