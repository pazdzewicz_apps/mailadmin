<?php
namespace Mailadmin\Controllers;

# Own Models
use Mailadmin\Models\Mailbox;
# Own Forms
use Mailadmin\Forms\MailboxForm;
use Mailadmin\Forms\MailboxNameForm;
use Mailadmin\Forms\MailboxPasswordForm;
# Other
use Phalcon\Paginator\Adapter\Model as PaginatorModel;

class MailboxController extends ControllerBase
{
  /**
  * Default action. Set the public layout (layouts/private.volt)
  */
  public function initialize()
  {
    $this->view->setVar('logged_in', is_array($this->auth->getIdentity()));
    $this->view->setTemplateBefore('private');
  }

  public function indexAction()
  {
    # Get Page for Paginator
    if(empty($this->request->get("page")))
    {
      $currentPage = 0;
    }
    else
    {
      $currentPage = (int) $this->request->get("page");
    }
    # Get Paginator Data
    $cache_key="mailadmin_mailbox_index.cache";
    $mailbox=$this->cache->get($cache_key);
    if($mailbox===null)
    {
      $mailbox=Mailbox::find();
      $this->cache->save($cache_key,$mailbox);
    }
    # Create Paginator
    $paginator=new PaginatorModel(
      [
        "data"  => $mailbox,
        "limit" => 10,
        "page"  => $currentPage,
      ]
    );
    $this->view->mailboxes=$paginator->getPaginate();
  }

  public function createAction($domain_id=null)
  {
    if($domain_id===null)
    {
      $this->flash->error("Domain not found!");
      return $this->dispatcher->forward([
          "action" => "index"
      ]);
    }
    $cache_key="mailadmin_domain_view_".$domain_id.".cache";
    $domain=$this->cache->get($cache_key);
    if($domain===null)
    {
      $domain=Domain::findFirst($domain_id);
      if($domain===false)
      {
        $this->flash->error("Domain not found!");
        return $this->dispatcher->forward([
            "action" => "index"
        ]);
      }
      $this->cache->save($cache_key,$domain);
    }

    $form = new MailboxForm();
    if($this->request->isPost())
    {
      if($form->isValid($this->request->getPost())!=false)
      {
        if($this->request->getPost('active')=="on")
        {
          $active=true;
        }
        else
        {
          $active=false;
        }
        $mailbox = new Mailbox([
          'name' => $this->request->getPost('name'),
          'quota' => $this->request->getPost('quota'),
          'username' => $this->request->getPost('local_part').'@'.$domain->getDomain(),
          'local_part' => $this->request->getPost('local_part'),
          'password' => $this->request->getPost('password'),
          'active' => $active,
          'domain_id' => $domain->getId(),
          'created' => 'now'
        ]);
        if($mailbox->create())
        {
          $this->flash->success("Mailbox created");
          if($mailbox->setPassword($this->request->getPost('password')))
          {
            $this->flash->success("Password set");
            $this->flash->success($mailbox->getMessages());
            // Clear Cache
            $this->cache->save($cache_key,null);
            $this->cache->save("mailadmin_mailbox_index.cache",null);
            $this->cache->save("mailadmin_mailbox_index_".$mailbox->getDomain()->getId().".cache",null);
            $this->cache->save("mailadmin_alias_index_".$mailbox->getDomain()->getId().".cache",null);
            // Forward to Mailbox site
            return $this->dispatcher->forward([
              'action' => 'view',
              'params' => [$mailbox->getId()]
            ]);
          }
          else
          {
            $this->flash->error("Password not set");
          }
        }
        $this->flash->error($mailbox->getMessages());
      }
      else
      {
        $this->flash->error("Submitted Data not valid");
        foreach ($form->getMessages() as $message) {
          // Displays all Warnings from the validation
          $this->flash->warning($message);
        }
      }
    }

    $this->view->domain=$domain;
    $this->view->form=$form;
  }

  public function viewAction($id=null)
  {
    if($id===null)
    {
      $this->flash->error("Mailbox not found!");
      return $this->dispatcher->forward([
          "action" => "index"
      ]);
    }
    $cache_key="mailadmin_mailbox_view_".$id.".cache";
    $mailbox=$this->cache->get($cache_key);
    if($mailbox===null)
    {
      $mailbox=Mailbox::findFirst($id);
      if($mailbox===false)
      {
        $this->flash->error("Mailbox not found!");
        return $this->dispatcher->forward([
            "action" => "index"
        ]);
      }
      $this->cache->save($cache_key,$mailbox);
    }
    $this->view->mailbox=$mailbox;
  }

  public function deleteAction($id=null)
  {
    if($id===null)
    {
      $this->flash->error("Mailbox not found!");
      return $this->dispatcher->forward([
          "action" => "index"
      ]);
    }
    $cache_key="mailadmin_mailbox_view_".$id.".cache";
    $mailbox=$this->cache->get($cache_key);
    if($mailbox===null)
    {
      $mailbox=Mailbox::findFirst($id);
      if($mailbox===false)
      {
        $this->flash->error("Mailbox not found!");
        return $this->dispatcher->forward([
            "action" => "index"
        ]);
      }
      $this->cache->save($cache_key,$mailbox);
    }
    if($this->request->isPost())
    {
      if($mailbox->delete())
      {
        $this->flash->success("Mailbox deleted!");
        $this->flash->success($mailbox->getMessages());
        // Clear Cache
        $this->cache->save($cache_key,null);
        $this->cache->save("mailadmin_mailbox_index.cache",null);
        $this->cache->save("mailadmin_mailbox_index_".$mailbox->getDomain()->getId().".cache",null);
        $this->cache->save("mailadmin_alias_index.cache",null);
        $this->cache->save("mailadmin_alias_index_".$mailbox->getDomain()->getId().".cache",null);
        return $this->dispatcher->forward([
            "controller" => "domain",
            "action" => "view",
            "params" => [$mailbox->getDomain()->getId()]
        ]);
      }
      else
      {
        $this->flash->error("Mailbox can't be deleted");
        foreach($mailbox->getMessages() as $message)
        {
          // Displays all Warnings from the validation
          $this->flash->warning($message);
        }
      }
    }
    $this->view->mailbox=$mailbox;
  }

  public function changeNameAction($id=null)
  {
    if($id===null)
    {
      $this->flash->error("Mailbox not found!");
      return $this->dispatcher->forward([
          "action" => "index"
      ]);
    }
    $cache_key="mailadmin_mailbox_view_".$id.".cache";
    $mailbox=$this->cache->get($cache_key);
    if($mailbox===null)
    {
      $mailbox=Mailbox::findFirst($id);
      if($mailbox===false)
      {
        $this->flash->error("Mailbox not found!");
        return $this->dispatcher->forward([
            "action" => "index"
        ]);
      }
      $this->cache->save($cache_key,$mailbox);
    }
    $form = new MailboxNameForm($mailbox);
    if($this->request->isPost())
    {
      if($form->isValid($this->request->getPost())!=false)
      {
        if($mailbox->setName($this->request->getPost('name')))
        {
          $this->flash->success("Mailbox Name changed!");
          // Clear Cache
          $this->cache->save($cache_key,null);
          $this->cache->save("mailadmin_mailbox_index.cache",null);
          $this->cache->save("mailadmin_mailbox_index_".$mailbox->getDomain()->getId().".cache",null);
          $this->cache->save("mailadmin_alias_index.cache",null);
          $this->cache->save("mailadmin_alias_index_".$mailbox->getDomain()->getId().".cache",null);
          return $this->dispatcher->forward([
              "controller" => "mailbox",
              "action" => "view",
              "params" => [$mailbox->getId()]
          ]);
        }
        else
        {
          $this->flash->error("Mailbox Name can't be changed");
          foreach($mailbox->getMessages() as $message)
          {
            // Displays all Warnings from the validation
            $this->flash->warning($message);
          }
        }
      }
      else
      {
        $this->flash->error("Mailbox Name can't be changed");
        foreach($mailbox->getMessages() as $message)
        {
          // Displays all Warnings from the validation
          $this->flash->warning($message);
        }
      }
    }
    $this->view->mailbox=$mailbox;
    $this->view->form=$form;
  }

  public function changePasswordAction($id=null)
  {
    if($id===null)
    {
      $this->flash->error("Mailbox not found!");
      return $this->dispatcher->forward([
          "action" => "index"
      ]);
    }
    $cache_key="mailadmin_mailbox_view_".$id.".cache";
    $mailbox=$this->cache->get($cache_key);
    if($mailbox===null)
    {
      $mailbox=Mailbox::findFirst($id);
      if($mailbox===false)
      {
        $this->flash->error("Mailbox not found!");
        return $this->dispatcher->forward([
            "action" => "index"
        ]);
      }
      $this->cache->save($cache_key,$mailbox);
    }
    $form = new MailboxPasswordForm();
    if($this->request->isPost())
    {
      if($form->isValid($this->request->getPost())!=false)
      {
        if($mailbox->setPassword($this->request->getPost('password')))
        {
          $this->flash->success("Mailbox Password changed!");
          // Clear Cache
          $this->cache->save($cache_key,null);
          $this->cache->save("mailadmin_mailbox_index.cache",null);
          $this->cache->save("mailadmin_mailbox_index_".$mailbox->getDomain()->getId().".cache",null);
          $this->cache->save("mailadmin_alias_index.cache",null);
          $this->cache->save("mailadmin_alias_index_".$mailbox->getDomain()->getId().".cache",null);
          return $this->dispatcher->forward([
              "controller" => "mailbox",
              "action" => "view",
              "params" => [$mailbox->getId()]
          ]);
        }
        else
        {
          $this->flash->error("Mailbox Password can't be changed");
          foreach($mailbox->getMessages() as $message)
          {
            // Displays all Warnings from the validation
            $this->flash->warning($message);
          }
        }
      }
      else
      {
        $this->flash->error("Mailbox Password can't be changed");
        foreach($mailbox->getMessages() as $message)
        {
          // Displays all Warnings from the validation
          $this->flash->warning($message);
        }
      }
    }
    $this->view->mailbox=$mailbox;
    $this->view->form=$form;
  }
}
