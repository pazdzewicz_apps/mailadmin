<?php
namespace Mailadmin\Controllers;

# Own Models
use Mailadmin\Models\Domain;
use Mailadmin\Models\Alias;
use Mailadmin\Models\Mailbox;
# Own Forms
use Mailadmin\Forms\DomainForm;
# Other
use Phalcon\Paginator\Adapter\Model as PaginatorModel;

class DomainController extends ControllerBase
{
  /**
  * Default action. Set the public layout (layouts/private.volt)
  */
  public function initialize()
  {
    $this->view->setVar('logged_in', is_array($this->auth->getIdentity()));
    $this->view->setTemplateBefore('private');
  }

  public function indexAction()
  {
    # Get Page for Paginator
    if(empty($this->request->get("page")))
    {
      $currentPage = 0;
    }
    else
    {
      $currentPage = (int) $this->request->get("page");
    }
    # Get Paginator Data
    $cache_key="mailadmin_domain_index.cache";
    $domains=$this->cache->get($cache_key);
    if($domains===null)
    {
      $domains=Domain::find();
      $this->cache->save($cache_key,$domains);
    }
    # Create Paginator
    $paginator=new PaginatorModel(
      [
        "data"  => $domains,
        "limit" => 10,
        "page"  => $currentPage,
      ]
    );
    $this->view->domains=$paginator->getPaginate();
  }

  public function createAction()
  {
    $form = new DomainForm();
    if($this->request->isPost())
    {
      if($form->isValid($this->request->getPost())!=false)
      {
        if($this->request->getPost('active') == "on")
        {
          $active=true;
        }
        else
        {
          $active=false;
        }
        if($this->request->getPost('backupmx') == "on")
        {
          $backupmx=true;
        }
        else
        {
          $backupmx=false;
        }
        $domain = new Domain([
          'domain' => $this->request->getPost('domain'),
          'description' => $this->request->getPost('description'),
          'max_aliases' => $this->request->getPost('max_aliases'),
          'max_mailboxes' => $this->request->getPost('max_mailboxes'),
          'max_quota' => $this->request->getPost('max_quota'),
          'active' => $active,
          'backupmx' => $backupmx,
          'created' => 'now'
        ]);
        if($domain->create())
        {
          $this->flash->success("Domain created");
          // Clear Cache
          $this->cache->save("mailadmin_domain_index.cache",null);
          // Forward to Domain site
          return $this->dispatcher->forward([
            'action' => 'view',
            'params' => [$domain->getId()]
          ]);
        }
        $this->flash->error($domain->getMessages());
      }
      else
      {
        $this->flash->error("Submitted Data not valid");
        foreach ($form->getMessages() as $message) {
          // Displays all Warnings from the validation
          $this->flash->warning($message);
        }
      }
    }
    $this->view->form=$form;
  }

  public function viewAction($id=null)
  {
    if($id===null)
    {
      $this->flash->error("Domain not found!");
      return $this->dispatcher->forward([
          "action" => "index"
      ]);
    }
    $cache_key="mailadmin_domain_view_".$id.".cache";
    $domain=$this->cache->get($cache_key);
    if($domain===null)
    {
      $domain=Domain::findFirst($id);
      if($domain===false)
      {
        $this->flash->error("Domain not found!");
        return $this->dispatcher->forward([
            "action" => "index"
        ]);
      }
      $this->cache->save($cache_key,$domain);
    }
    # Get Page for Alias Paginator
    if(empty($this->request->get("aliaspage")))
    {
      $aliasCurrentPage = 0;
    }
    else
    {
      $aliasCurrentPage = (int) $this->request->get("aliaspage");
    }
    # Get Alias Paginator Data
    $cache_key="mailadmin_alias_index_".$domain->getId().".cache";
    $alias=$this->cache->get($cache_key);
    if($alias===null)
    {
      $alias=Alias::find('domain_id = '.$domain->getId());
      $this->cache->save($cache_key,$alias);
    }
    # Create Alias Paginator
    $aliasPaginator=new PaginatorModel(
      [
        "data"  => $alias,
        "limit" => 10,
        "page"  => $aliasCurrentPage,
      ]
    );
    # Get Page for Mailbox Paginator
    if(empty($this->request->get("mailboxpage")))
    {
      $mailboxCurrentPage = 0;
    }
    else
    {
      $mailboxCurrentPage = (int) $this->request->get("mailboxpage");
    }
    # Get Mailbox Paginator Data
    $cache_key="mailadmin_mailbox_index_".$domain->getId().".cache";
    $mailbox=$this->cache->get($cache_key);
    if($mailbox===null)
    {
      $mailbox=Mailbox::find('domain_id = '.$domain->getId());
      $this->cache->save($cache_key,$mailbox);
    }
    # Create Mailbox Paginator
    $mailboxPaginator=new PaginatorModel(
      [
        "data"  => $mailbox,
        "limit" => 10,
        "page"  => $mailboxCurrentPage,
      ]
    );
    $this->view->mailboxes=$mailboxPaginator->getPaginate();
    $this->view->aliases=$aliasPaginator->getPaginate();
    $this->view->domain=$domain;
  }

  public function deleteAction($id=null)
  {
    if($id===null)
    {
      $this->flash->error("Domain not found!");
      return $this->dispatcher->forward([
          "action" => "index"
      ]);
    }
    $cache_key="mailadmin_domain_view_".$id.".cache";
    $domain=$this->cache->get($cache_key);
    if($domain===null)
    {
      $domain=Domain::findFirst($id);
      if($domain===false)
      {
        $this->flash->error("Domain not found!");
        return $this->dispatcher->forward([
            "action" => "index"
        ]);
      }
      $this->cache->save($cache_key,$domain);
    }
    if($this->request->isPost())
    {
      if($domain->delete())
      {
        $this->flash->success("Domain deleted!");
        // Clear Cache
        $this->cache->save($cache_key,null);
        $this->cache->save("mailadmin_domain_index.cache",null);
        return $this->dispatcher->forward([
            "action" => "index"
        ]);
      }
      else
      {
        $this->flash->error("Domain can't be deleted");
        foreach($domain->getMessages() as $message)
        {
          // Displays all Warnings from the validation
          $this->flash->warning($message);
        }
      }
    }
    $this->view->domain=$domain;
  }
}
