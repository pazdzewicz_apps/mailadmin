<div class="row">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-lg-12">
            <h1><i class="fas fa-history"></i> Cache Key "{{key}}"</h1>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            {{ content() }}
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            <p>{{ link_to('cache/delete/' ~ key, '<i class="fas fa-trash"></i> Delete Key', 'class':'btn btn-danger form-control') }}</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-lg-12">
            <h5 class="card-title">Cache Data:</h5>
          </div>
          <div class="col-lg-12">
            <pre>{{ cached }}</pre>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
