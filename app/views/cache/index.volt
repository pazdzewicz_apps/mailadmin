<div class="row">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-lg-12">
            <h1><i class="fas fa-history"></i> Cache Key List</h1>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            {{ content() }}
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            <p>{{ link_to('cache/delete/', '<i class="fas fa-trash"></i> Delete all Keys', 'class':'btn btn-danger form-control') }}</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
{% if caches.total_pages > 0 %}
  <div class="row">
    <div class="col-lg-12">
      <div class="card">
        <div class="card-body">
          <div class="row">
            <div class="col-lg-12">
              <table class="table table-borderless table-striped">
                <thead>
                  <tr>
                    <th scope="col">Cache Key</th>
                    <th scope="col"></th>
                  </tr>
                </thead>
                {% for cached in caches.items %}
                  <tbody>
                    <tr>
                      <th scope="col">{{cached}}</th>
                      <td>{{ link_to('cache/view/' ~ cached, '<i class="fas fa-info-circle"></i> Details', 'class':'btn btn-primary form-control') }}</td>
                      <td>{{ link_to('cache/delete/' ~ cached, '<i class="fas fa-trash"></i> Delete', 'class':'btn btn-danger form-control') }}</td>
                    </tr>
                  </tbody>
                {% endfor %}
              </table>
              {% if caches.total_pages > 1 %}
                <div class="row">
                  <div class="col-lg-2">
                    {{ link_to("cache/index", '<i class="fas fa-angle-double-left"></i> First', "class": "btn btn-primary form-control") }}
                  </div>
                  <div class="col-lg-2">
                    {{ link_to("cache/index?page=" ~ caches.before, '<i class="fas fa-angle-left"></i> Previous', "class": "btn btn-primary form-control") }}
                  </div>
                  <div class="col-lg-4">
                    <p class="form-control">{{ caches.current }}/{{ caches.total_pages }}</p>
                  </div>
                  <div class="col-lg-2">
                    {{ link_to("cache/index?page=" ~ caches.next, 'Next <i class="fas fa-angle-right"></i>', "class": "btn btn-primary form-control") }}
                  </div>
                  <div class="col-lg-2">
                    {{ link_to("cache/index?page=" ~ caches.last, 'Last <i class="fas fa-angle-double-right"></i>', "class": "btn btn-primary form-control") }}
                  </div>
                </div>
              {% endif %}
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
{% endif %}
