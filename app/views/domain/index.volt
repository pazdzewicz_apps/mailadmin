<div class="row">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-lg-12">
            <h1><i class="fas fa-at"></i> Domain Overview</h1>
            <hr>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            {{ content() }}
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            {{ link_to("domain/create/", '<i class="fas fa-plus"></i> Create Domain', "class": "btn btn-success form-control") }}
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-body">
        <div class="row">
					<div class="col-lg-12">
				    {% for domain in domains.items %}
				      {% if loop.first %}
				      <table class="table table-borderless table-striped" align="center">
				        <thead>
				          <tr>
				            <th>Domain Name</th>
				            <th>Active?</th>
                    <th>Backup MX?</th>
                    <th>Last Modified</th>
                    <th colspan="2"></th>
				          </tr>
				        </thead>
				        <tbody>
				      {% endif %}
				      <tr>
				        <th>{{ domain.getDomain() }}</th>
                <td>{{ domain.getActive() == true ? 'Yes' : 'No' }}</td>
                <td>{{ domain.getBackupMX() == true ? 'Yes' : 'No' }}</td>
				        <td>{{ domain.getModified() == null ? domain.getCreated() : domain.getModified() }}</td>
				        <td>{{ link_to("domain/view/" ~ domain.getId(), '<i class="fas fa-search"></i> View', "class": "btn btn-primary form-control") }}</td>
				        <td>{{ link_to("domain/delete/" ~ domain.getId(), '<i class="fas fa-trash"></i> Delete', "class": "btn btn-danger form-control") }}</td>
				      </tr>
				      {% if loop.last %}
				      </tbody>
				    </table>
				    {% endif %}
				    {% else %}
				        No domains are recorded
				    {% endfor %}
					</div>
				</div>
        {% if domains.total_pages > 1 %}
  				<div class="row">
  				  <div class="col-lg-2">
  				    {{ link_to("domain/index", '<i class="fas fa-angle-double-left"></i> First', "class": "btn btn-primary form-control") }}
  				  </div>
  				  <div class="col-lg-2">
  				    {{ link_to("domain/index?page=" ~ domains.before, '<i class="fas fa-angle-left"></i> Previous', "class": "btn btn-primary form-control") }}
  				  </div>
  				  <div class="col-lg-4">
  				    <p class="form-control">{{ domains.current }}/{{ domains.total_pages }}</p>
  				  </div>
  				  <div class="col-lg-2">
  				    {{ link_to("domain/index?page=" ~ domains.next, 'Next <i class="fas fa-angle-right"></i>', "class": "btn btn-primary form-control") }}
  				  </div>
  				  <div class="col-lg-2">
  				    {{ link_to("domain/index?page=" ~ domains.last, 'Last <i class="fas fa-angle-double-right"></i>', "class": "btn btn-primary form-control") }}
  				  </div>
  				</div>
        {% endif %}
      </div>
    </div>
  </div>
</div>
