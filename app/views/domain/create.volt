<div class="row">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-lg-12">
            <h1><i class="fas fa-at"></i> Create new Domain</h1>
            <hr>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            {{ content() }}
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-lg-12">
            <form method="post">
              <div class="row">
                <div class="col-6">
                  <div class="row">
                    <div class="col-12">
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text" id="domain">{{ form.label('domain') }}</span>
                        </div>
                        {{ form.render('domain') }}
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-12">
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text" id="description">{{ form.label('description') }}</span>
                        </div>
                        {{ form.render('description') }}
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-6">
                  <div class="row">
                    <div class="col-12">
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text" id="max_aliases">{{ form.label('max_aliases') }}</span>
                        </div>
                        {{ form.render('max_aliases') }}
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-12">
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text" id="max_mailboxes">{{ form.label('max_mailboxes') }}</span>
                        </div>
                        {{ form.render('max_mailboxes') }}
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-12">
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text" id="max_quota">{{ form.label('max_quota') }}</span>
                        </div>
                        {{ form.render('max_quota') }}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-6">
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text" id="active">{{ form.render('active') }}</span>
                    </div>
                    <p class="form-control">{{ form.label('active') }}</p>
                  </div>
                </div>
                <div class="col-6">
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text" id="backupmx">{{ form.render('backupmx') }}</span>
                    </div>
                    <p class="form-control">{{ form.label('backupmx') }}</p>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-12">
                  {{ form.render('go') }}
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
