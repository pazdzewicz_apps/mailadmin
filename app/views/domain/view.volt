<div class="row">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-lg-12">
            <h1><i class="fas fa-at"></i> Domain Information for {{ link_to("domain/view/" ~ domain.getId(), domain.getDomain()) }}</h1>
            <hr>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            {{ content() }}
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            <table class="table table-borderless table-striped" align="center">
              <tbody>
                <tr>
                  <th>Created</th>
                  <td>{{ domain.getCreated() }}</td>
                </tr>
                <tr>
                  <th>Last Modified</th>
                  <td>{{ domain.getModified() == null ? domain.getCreated() : domain.getModified() }}</td>
                </tr>
                <tr>
                  <th>Max Aliases</th>
                  <td>{{ domain.getMaxAliases() == 0 ? 'Unlimited' : domain.getMaxAliases() }}</td>
                </tr>
                <tr>
                  <th>Max Mailboxes</th>
                  <td>{{ domain.getMaxMailboxes() == 0 ? 'Unlimited' : domain.getMaxMailboxes() }}</td>
                </tr>
                <tr>
                  <th>Max Quota</th>
                  <td>{{ domain.getMaxQuota() == 0 ? 'Unlimited' : domain.getMaxQuota() }}</td>
                </tr>
                <tr>
                  <th>Active</th>
                  <td>{{ domain.getActive() == true ? 'Yes' : 'No' }}</td>
                </tr>
                <tr>
                  <th>Backup MX</th>
                  <td>{{ domain.getBackupMX() == true ? 'Yes' : 'No' }}</td>
                </tr>
                <tr>
                  <th>Transport</th>
                  <td>{{ domain.getTransport() }}</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-lg-12">
            <h3><i class="fas fa-user-secret"></i> Alias Overview</h3>
            <hr>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            {{ link_to("alias/create/" ~ domain.getId(), '<i class="fas fa-user-secret"></i> Create Alias', "class": "btn btn-success form-control") }}
          </div>
        </div>
        <div class="row">
					<div class="col-lg-12">
				    {% for alias in aliases.items %}
				      {% if loop.first %}
				      <table class="table table-borderless table-striped" align="center">
				        <thead>
				          <tr>
				            <th>Email Address</th>
                    <th>Destination Address</th>
				            <th>Active?</th>
                    <th>Last Modified</th>
                    <th colspan="2"></th>
				          </tr>
				        </thead>
				        <tbody>
				      {% endif %}
				      <tr>
				        <th>{{ alias.getAddress() }}</th>
				        <td>{{ alias.getGoto() }}</td>
                <td>{{ alias.getActive() == true ? 'Yes' : 'No' }}</td>
				        <td>{{ alias.getModified() == null ? alias.getCreated() : alias.getModified() }}</td>
				        <td>{{ link_to("alias/view/" ~ alias.getId(), '<i class="fas fa-search"></i> View', "class": "btn btn-primary form-control") }}</td>
				        <td>{{ link_to("alias/delete/" ~ alias.getId(), '<i class="fas fa-trash"></i> Delete', "class": "btn btn-danger form-control") }}</td>
				      </tr>
				      {% if loop.last %}
				      </tbody>
				    </table>
				    {% endif %}
				    {% else %}
				        No aliases are recorded
				    {% endfor %}
					</div>
				</div>
        {% if aliases.total_pages > 1 %}
  				<div class="row">
  				  <div class="col-lg-2">
  				    {{ link_to("domain/view/" ~ domain.getId(), '<i class="fas fa-angle-double-left"></i> First', "class": "btn btn-primary form-control") }}
  				  </div>
  				  <div class="col-lg-2">
  				    {{ link_to("domain/view/"  ~ domain.getId() ~ "?aliaspage=" ~ aliases.before, '<i class="fas fa-angle-left"></i> Previous', "class": "btn btn-primary form-control") }}
  				  </div>
  				  <div class="col-lg-4">
  				    <p class="form-control">{{ aliases.current }}/{{ aliases.total_pages }}</p>
  				  </div>
  				  <div class="col-lg-2">
  				    {{ link_to("domain/view/"  ~ domain.getId() ~ "?aliaspage=" ~ aliases.next, 'Next <i class="fas fa-angle-right"></i>', "class": "btn btn-primary form-control") }}
  				  </div>
  				  <div class="col-lg-2">
  				    {{ link_to("domain/view/"  ~ domain.getId() ~ "?aliaspage=" ~ aliases.last, 'Last <i class="fas fa-angle-double-right"></i>', "class": "btn btn-primary form-control") }}
  				  </div>
  				</div>
        {% endif %}
      </div>
    </div>
  </div>
</div>


<div class="row">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-lg-12">
            <h1><i class="far fa-envelope"></i> Mailbox Overview</h1>
            <hr>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            {{ link_to("mailbox/create/" ~ domain.getId(), '<i class="far fa-envelope"></i> Create Mailbox', "class": "btn btn-success form-control") }}
          </div>
        </div>
        <div class="row">
					<div class="col-lg-12">
				    {% for mailbox in mailboxes.items %}
				      {% if loop.first %}
				      <table class="table table-borderless table-striped" align="center">
				        <thead>
				          <tr>
				            <th>Name</th>
				            <th>Email Address</th>
				            <th>Active?</th>
                    <th>Last Modified</th>
                    <th colspan="2"></th>
				          </tr>
				        </thead>
				        <tbody>
				      {% endif %}
				      <tr>
				        <th>{{ mailbox.getName() }}</th>
				        <td>{{ mailbox.getUsername() }}</td>
                <td>{{ mailbox.getActive() == true ? 'Yes' : 'No' }}</td>
				        <td>{{ mailbox.getModified() == null ? mailbox.getCreated() : mailbox.getModified() }}</td>
				        <td>{{ link_to("mailbox/view/" ~ mailbox.getId(), '<i class="fas fa-search"></i> View', "class": "btn btn-primary form-control") }}</td>
				        <td>{{ link_to("mailbox/delete/" ~ mailbox.getId(), '<i class="fas fa-trash"></i> Delete', "class": "btn btn-danger form-control") }}</td>
				      </tr>
				      {% if loop.last %}
				      </tbody>
				    </table>
				    {% endif %}
				    {% else %}
				        No mailboxes are recorded
				    {% endfor %}
					</div>
				</div>
        {% if mailboxes.total_pages > 1 %}
  				<div class="row">
  				  <div class="col-lg-2">
  				    {{ link_to("domain/view/" ~ domain.getId(), '<i class="fas fa-angle-double-left"></i> First', "class": "btn btn-primary form-control") }}
  				  </div>
  				  <div class="col-lg-2">
  				    {{ link_to("domain/view/"  ~ domain.getId() ~ "?mailboxpage=" ~ mailboxes.before, '<i class="fas fa-angle-left"></i> Previous', "class": "btn btn-primary form-control") }}
  				  </div>
  				  <div class="col-lg-4">
  				    <p class="form-control">{{ mailboxes.current }}/{{ mailboxes.total_pages }}</p>
  				  </div>
  				  <div class="col-lg-2">
  				    {{ link_to("domain/view/"  ~ domain.getId() ~ "?mailboxpage=" ~ mailboxes.next, 'Next <i class="fas fa-angle-right"></i>', "class": "btn btn-primary form-control") }}
  				  </div>
  				  <div class="col-lg-2">
  				    {{ link_to("domain/view/"  ~ domain.getId() ~ "?mailboxpage=" ~ mailboxes.last, 'Last <i class="fas fa-angle-double-right"></i>', "class": "btn btn-primary form-control") }}
  				  </div>
  				</div>
        {% endif %}
      </div>
    </div>
  </div>
</div>
