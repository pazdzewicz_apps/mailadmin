<div class="row">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-lg-12">
            <h1><i class="fas fa-user-secret"></i> Alias Information for {{ link_to("alias/view/" ~ alias.getId(), alias.getAddress() ~ " to " ~ alias.getGoto()) }}</h1>
            <hr>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            {{ content() }}
          </div>
        </div>
        <div class="row">
          <div class="col-6">
            {{ link_to("alias/changeGoto/" ~ alias.getId(), '<i class="fas fa-arrow-right"></i> Update Destination', "class": "btn btn-primary form-control") }}
          </div>
          <div class="col-6">
            {{ link_to("alias/delete/" ~ alias.getId(), '<i class="fas fa-trash"></i> Delete Alias', "class": "btn btn-danger form-control") }}
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-lg-12">
            <table class="table table-borderless table-striped" align="center">
              <tbody>
                <tr>
                  <th>Created</th>
                  <td>{{ alias.getCreated() }}</td>
                </tr>
                <tr>
                  <th>Last Modified</th>
                  <td>{{ alias.getModified() == null ? alias.getCreated() : alias.getModified() }}</td>
                </tr>
                <tr>
                  <th>Active</th>
                  <td>{{ alias.getActive() == true ? 'Yes' : 'No' }}</td>
                </tr>
                <tr>
                  <th>Target</th>
                  <td>{{ alias.getAddress() }}</td>
                </tr>
                <tr>
                  <th>Destination</th>
                  <td>{{ alias.getGoto() }}</td>
                </tr>
                <tr>
                  <th>Domain</th>
                  <td>{{ link_to("domain/view/" ~ alias.getDomain().getId(), alias.getDomain().getDomain())}}</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
