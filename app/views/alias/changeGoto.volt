<div class="row">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-lg-12">
            <h1><i class="fas fa-user-secret"></i> Change Destination for Alias{{ link_to("alias/view/" ~ alias.getId(), alias.getAddress() ~ " to " ~ alias.getGoto()) }}</h1>
            <hr>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            {{ content() }}
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-lg-12">
            <form method="post">
              <div class="row">
                <div class="col-12">
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text" id="goto">{{ form.label('goto') }}</span>
                    </div>
                    {{ form.render('goto') }}
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-12">
                  {{ form.render('go') }}
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
