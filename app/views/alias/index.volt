<div class="row">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-lg-12">
            <h1><i class="fas fa-user-secret"></i> Alias Overview</h1>
            <hr>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            {{ content() }}
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-body">
        <div class="row">
					<div class="col-lg-12">
				    {% for alias in aliases.items %}
				      {% if loop.first %}
				      <table class="table table-borderless table-striped" align="center">
				        <thead>
				          <tr>
				            <th>Email Address</th>
                    <th>Destination Address</th>
				            <th>Active?</th>
                    <th>Last Modified</th>
                    <th colspan="2"></th>
				          </tr>
				        </thead>
				        <tbody>
				      {% endif %}
				      <tr>
				        <th>{{ alias.getAddress() }}</th>
				        <td>{{ alias.getGoto() }}</td>
                <td>{{ alias.getActive() == true ? 'Yes' : 'No' }}</td>
				        <td>{{ alias.getModified() == null ? alias.getCreated() : alias.getModified() }}</td>
				        <td>{{ link_to("alias/view/" ~ alias.getId(), '<i class="fas fa-search"></i> View', "class": "btn btn-primary form-control") }}</td>
				        <td>{{ link_to("alias/delete/" ~ alias.getId(), '<i class="fas fa-trash"></i> Delete', "class": "btn btn-danger form-control") }}</td>
				      </tr>
				      {% if loop.last %}
				      </tbody>
				    </table>
				    {% endif %}
				    {% else %}
				        No aliases are recorded
				    {% endfor %}
					</div>
				</div>
				<div class="row">
				  <div class="col-lg-2">
				    {{ link_to("alias/index", '<i class="fas fa-angle-double-left"></i> First', "class": "btn btn-primary form-control") }}
				  </div>
				  <div class="col-lg-2">
				    {{ link_to("alias/index?page=" ~ aliases.before, '<i class="fas fa-angle-left"></i> Previous', "class": "btn btn-primary form-control") }}
				  </div>
				  <div class="col-lg-4">
				    <p class="form-control">{{ aliases.current }}/{{ aliases.total_pages }}</p>
				  </div>
				  <div class="col-lg-2">
				    {{ link_to("alias/index?page=" ~ aliases.next, 'Next <i class="fas fa-angle-right"></i>', "class": "btn btn-primary form-control") }}
				  </div>
				  <div class="col-lg-2">
				    {{ link_to("alias/index?page=" ~ aliases.last, 'Last <i class="fas fa-angle-double-right"></i>', "class": "btn btn-primary form-control") }}
				  </div>
				</div>
      </div>
    </div>
  </div>
</div>
