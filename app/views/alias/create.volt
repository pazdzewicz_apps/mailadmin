<div class="row">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-lg-12">
            <h1><i class="fas fa-user-secret"></i> Create Alias for Domain {{ link_to("domain/view/" ~ domain.getId(), domain.getDomain()) }}</h1>
            <hr>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            {{ content() }}
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-lg-12">
            <form method="post">
              <div class="row">
                <div class="col-12">
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text" id="local_part">{{ form.label('local_part') }}</span>
                    </div>
                    {{ form.render('local_part') }}
                    <div class="input-group-append">
                      <span class="input-group-text" id="local_part">@{{ domain.getDomain() }}</span>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-12">
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text" id="goto">{{ form.label('goto') }}</span>
                    </div>
                    {{ form.render('goto') }}
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-12">
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text" id="active">{{ form.render('active') }}</span>
                    </div>
                    <p class="form-control">{{ form.label('active') }}</p>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-12">
                  {{ form.render('go') }}
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
