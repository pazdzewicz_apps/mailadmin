<div class="row">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-lg-12">
            <h1><i class="fas fa-user-secret"></i> Delete Alias {{ alias.getAddress() }} to {{ alias.getGoto() }}</h1>
            <hr>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            {{ content() }}
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-12">
            <p>Are you sure you want to delete the alias {{ alias.getAddress() }}?</p>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            <form method="post">
              <div class="row">
                <div class="col-12">
                  <button type="submit" class="btn btn-danger form-control">Yes, Delete</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
