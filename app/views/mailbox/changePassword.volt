<div class="row">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-lg-12">
            <h1><i class="far fa-envelope"></i> Change Password for Mailbox {{ link_to("mailbox/view/" ~ mailbox.getId(), mailbox.getName() ~ " - " ~ mailbox.getUsername()) }}</h1>
            <hr>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            {{ content() }}
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-lg-12">
            <form method="post">
              <div class="row">
                <div class="col-6">
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text" id="password">{{ form.label('password') }}</span>
                    </div>
                    {{ form.render('password') }}
                  </div>
                </div>
                <div class="col-6">
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text" id="confirmPassword">{{ form.label('confirmPassword') }}</span>
                    </div>
                    {{ form.render('confirmPassword') }}
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-12">
                  {{ form.render('go') }}
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
