<div class="row">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-lg-12">
            <h1><i class="far fa-envelope"></i> Delete Mailbox {{ mailbox.getUsername() }}</h1>
            <hr>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            {{ content() }}
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-12">
            <p>Are you sure you want to delete the Mailbox {{ mailbox.getUsername() }}?</p>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            <form method="post">
              <div class="row">
                <div class="col-12">
                  <button type="submit" class="btn btn-danger form-control">Yes, Delete</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
