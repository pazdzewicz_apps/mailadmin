<div class="row">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-lg-12">
            <h1><i class="far fa-envelope"></i> Mailbox Information for {{ link_to("mailbox/view/" ~ mailbox.getId(), mailbox.getName() ~ " - " ~ mailbox.getUsername()) }}</h1>
            <hr>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            {{ content() }}
          </div>
        </div>
        <div class="row">
          <div class="col-4">
            {{ link_to("mailbox/changeName/" ~ mailbox.getId(), '<i class="fas fa-user-cog"></i> Update Name', "class": "btn btn-primary form-control") }}
          </div>
          <div class="col-4">
            {{ link_to("mailbox/changePassword/" ~ mailbox.getId(), '<i class="fas fa-user-edit"></i> Update Password', "class": "btn btn-primary form-control") }}
          </div>
          <div class="col-4">
            {{ link_to("mailbox/delete/" ~ mailbox.getId(), '<i class="fas fa-trash"></i> Delete Mailbox', "class": "btn btn-danger form-control") }}
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-lg-12">
            <table class="table table-borderless table-striped" align="center">
              <tbody>
                <tr>
                  <th>Created</th>
                  <td>{{ mailbox.getCreated() }}</td>
                </tr>
                <tr>
                  <th>Last Modified</th>
                  <td>{{ mailbox.getModified() == null ? mailbox.getCreated() : mailbox.getModified() }}</td>
                </tr>
                <tr>
                  <th>Quota</th>
                  <td>{{ mailbox.getQuota() == 0 ? 'Unlimited' : mailbox.getQuota() }}</td>
                </tr>
                <tr>
                  <th>Active</th>
                  <td>{{ mailbox.getActive() == true ? 'Yes' : 'No' }}</td>
                </tr>
                <tr>
                  <th>Homedir</th>
                  <td>{{ mailbox.getHomedir() }}</td>
                </tr>
                <tr>
                  <th>Maildir</th>
                  <td>{{ mailbox.getMaildir() }}</td>
                </tr>
                <tr>
                  <th>Mailbox User ID</th>
                  <td>{{ mailbox.getUID() }}</td>
                </tr>
                <tr>
                  <th>Mailbox Group ID</th>
                  <td>{{ mailbox.getGID() }}</td>
                </tr>
                <tr>
                  <th>Domain</th>
                  <td>{{ link_to("domain/view/" ~ mailbox.getDomain().getId(), mailbox.getDomain().getDomain())}}</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
