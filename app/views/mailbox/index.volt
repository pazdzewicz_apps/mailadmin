<div class="row">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-lg-12">
            <h1><i class="far fa-envelope"></i> Mailbox Overview</h1>
            <hr>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            {{ content() }}
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-body">
        <div class="row">
					<div class="col-lg-12">
				    {% for mailbox in mailboxes.items %}
				      {% if loop.first %}
				      <table class="table table-borderless table-striped" align="center">
				        <thead>
				          <tr>
				            <th>Name</th>
				            <th>Email Address</th>
				            <th>Active?</th>
                    <th>Last Modified</th>
                    <th colspan="2"></th>
				          </tr>
				        </thead>
				        <tbody>
				      {% endif %}
				      <tr>
				        <th>{{ mailbox.getName() }}</th>
				        <td>{{ mailbox.getUsername() }}</td>
                <td>{{ mailbox.getActive() == true ? 'Yes' : 'No' }}</td>
				        <td>{{ mailbox.getModified() == null ? mailbox.getCreated() : mailbox.getModified() }}</td>
				        <td>{{ link_to("mailbox/view/" ~ mailbox.getId(), '<i class="fas fa-search"></i> View', "class": "btn btn-primary form-control") }}</td>
				        <td>{{ link_to("mailbox/delete/" ~ mailbox.getId(), '<i class="fas fa-trash"></i> Delete', "class": "btn btn-danger form-control") }}</td>
				      </tr>
				      {% if loop.last %}
				      </tbody>
				    </table>
				    {% endif %}
				    {% else %}
				        No mailboxes are recorded
				    {% endfor %}
					</div>
				</div>
				<div class="row">
				  <div class="col-lg-2">
				    {{ link_to("mailbox/index", '<i class="fas fa-angle-double-left"></i> First', "class": "btn btn-primary form-control") }}
				  </div>
				  <div class="col-lg-2">
				    {{ link_to("mailbox/index?page=" ~ mailboxes.before, '<i class="fas fa-angle-left"></i> Previous', "class": "btn btn-primary form-control") }}
				  </div>
				  <div class="col-lg-4">
				    <p class="form-control">{{ mailboxes.current }}/{{ mailboxes.total_pages }}</p>
				  </div>
				  <div class="col-lg-2">
				    {{ link_to("mailbox/index?page=" ~ mailboxes.next, 'Next <i class="fas fa-angle-right"></i>', "class": "btn btn-primary form-control") }}
				  </div>
				  <div class="col-lg-2">
				    {{ link_to("mailbox/index?page=" ~ mailboxes.last, 'Last <i class="fas fa-angle-double-right"></i>', "class": "btn btn-primary form-control") }}
				  </div>
				</div>
      </div>
    </div>
  </div>
</div>
