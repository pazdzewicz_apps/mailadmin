<div class="row">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-lg-12">
            <h1><i class="far fa-envelope"></i> Create Mailbox for Domain {{ link_to("domain/view/" ~ domain.getId(), domain.getDomain()) }}</h1>
            <hr>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            {{ content() }}
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-lg-12">
            <form method="post">
              <div class="row">
                <div class="col-6">
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text" id="local_part">{{ form.label('local_part') }}</span>
                    </div>
                    {{ form.render('local_part') }}
                    <div class="input-group-append">
                      <span class="input-group-text" id="local_part">@{{ domain.getDomain() }}</span>
                    </div>
                  </div>
                </div>
                <div class="col-6">
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text" id="name">{{ form.label('name') }}</span>
                    </div>
                    {{ form.render('name') }}
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-6">
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text" id="password">{{ form.label('password') }}</span>
                    </div>
                    {{ form.render('password') }}
                  </div>
                </div>
                <div class="col-6">
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text" id="confirmPassword">{{ form.label('confirmPassword') }}</span>
                    </div>
                    {{ form.render('confirmPassword') }}
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-6">
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text" id="active">{{ form.render('active') }}</span>
                    </div>
                    <p class="form-control">{{ form.label('active') }}</p>
                  </div>
                </div>
                <div class="col-6">
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text" id="quota">{{ form.label('quota') }}</span>
                    </div>
                    {{ form.render('quota') }}
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-12">
                  {{ form.render('go') }}
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
