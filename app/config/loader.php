<?php

use Phalcon\Loader;

$loader = new Loader();

/**
 * We're a registering a set of directories taken from the configuration file
 */
$loader->registerNamespaces([
    'Mailadmin\Models'      => $config->application->modelsDir,
    'Mailadmin\Controllers' => $config->application->controllersDir,
    'Mailadmin\Forms'       => $config->application->formsDir,
    'Mailadmin'             => $config->application->libraryDir
]);

$loader->register();

// Use composer autoloader to load vendor classes
require_once BASE_PATH . '/vendor/autoload.php';
