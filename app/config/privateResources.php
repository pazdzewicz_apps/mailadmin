<?php
use Phalcon\Config;
use Phalcon\Logger;
return new Config([
  'privateResources' => [
    'users' => [
      'index',
      'search',
      'edit',
      'create',
      'delete',
      'changePassword'
    ],
    'profiles' => [
      'index',
      'search',
      'edit',
      'create',
      'delete'
    ],
    'permissions' => [
      'index'
    ],
    'mailbox' => [
      'index',
      'create',
      'view',
      'delete',
      'changeName',
      'changePassword'
    ],
    'alias' => [
      'index',
      'create',
      'view',
      'delete',
      'changeGoto'
    ],
    'domain' => [
      'index',
      'create',
      'view',
      'delete'
    ],
    'cache' => [
      'index',
      'delete',
      'view'
    ]
  ]
]);
