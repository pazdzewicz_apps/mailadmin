# Peppermint.Cloud Mailadmin

Simple and easy to use mail administration web ui.

This web ui is written in the ultra fast [phalcon framework](https://phalcon.io/en-us) with redis session, model and metadata cache. This tool is ultra fast and needs little to no resources.

## Features:

- Create public or private use Domains. Public domains are usable for signup (e.g. freemailer usage). Private Domains are tied to an admin user.
- User Management. A new Mailbox also creates a new user in the application with permissions to change the password.
- Permissions: Mailbox User (can edit own mailbox password, can create aliases from public domains), Domain Admin (can manage private domains), Mail Admin (can manage private & public domains)

License
-------

Peppermint.Cloud Mailadmin is open-sourced software licensed under the New BSD License. Please read the License located in `LICENSE.md`
